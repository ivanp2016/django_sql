from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Product(models.Model):
    name = models.CharField(max_length=256)
    price = models.IntegerField(default=0)
    class Meta():
        db_table = 'product'

    def __str__(self):
        return self.name

class Comment(models.Model):
    product_id = models.ForeignKey(Product)
    text_short = models.CharField(max_length=256)
    text = models.TextField(max_length=2000)

    class Meta():
        db_table = 'comment'

    def __str__(self):
        return self.text_short

